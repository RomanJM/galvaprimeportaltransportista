export const UrlServer = "http://proveedores.galvaprime.com/api_trans_prd";
//export const UrlServer = "http://proveedores.galvaprime.com/api_trans_qas";
//export const UrlServer = "http://localhost:1366/";

//GENERAL
export const LogIn = UrlServer.concat('/api/AAcceso');
export const LeerTransportistas = UrlServer.concat('/api/CatalogoSAP/LeerTransportistas');
export const LeerCamiones = UrlServer.concat('/api/CatalogoSAP/camionesTransportistas/');
export const ImprimirSalida = UrlServer.concat('/Documentos/ImprimeSalida/?folio=');
export const ImprimirEntrada = UrlServer.concat('/Documentos/ImprimeEntrada/?folio=');

//ENTRADAS
export const GuardarEntrada = UrlServer.concat('/api/Entrada/Guardar');

//SALIDAS
export const GuardarSalida = UrlServer.concat('/api/Salida/Guardar');
export const ConsultaFolios = UrlServer.concat('/api/Salida/Pendientes/');


