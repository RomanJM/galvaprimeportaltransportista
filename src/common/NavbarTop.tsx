import React from 'react';
import {  Navbar, Container, Nav, NavDropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { NavLink } from 'reactstrap';
import { RouteComponentProps } from 'react-router';
export default class NavbarTop extends React.Component<any, any>{
    public render(){
        var usuario =  window.localStorage.getItem("NombreUsuario");
        return <Navbar collapseOnSelect expand="lg" bg="light" variant="light" sticky="top" style={{marginBottom:'1em'}} >
                    <Container>
                    <Navbar.Brand href="/Inicio">
                        <img
                            src="images/Logo-Brand.png"
                            width="180"
                            height="40"
                            className="d-inline-block align-top"
                            alt="React Bootstrap logo"
                        />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        <NavDropdown title={"Registros"} id="collasible-nav-dropdown">
                            <NavDropdown.Item >
                                <NavLink tag={Link} to={`/Entrada`}  > Entrada </NavLink>
                            </NavDropdown.Item>
                            <NavDropdown.Item >
                                <NavLink tag={Link} to={`/Salida`}  > Salida </NavLink>
                            </NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title={"Cargas"} id="collasible-nav-dropdown">
                            <NavDropdown.Item >
                                <NavLink tag={Link} to={`/CargarCamion`}  > Carga de camión </NavLink>
                            </NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title={"Descargas"} id="collasible-nav-dropdown">
                            <NavDropdown.Item >
                                <NavLink tag={Link} to={`/DescargarCamion`}  > Descargar camion </NavLink>
                            </NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title={"Consultar"} id="collasible-nav-dropdown">
                        <NavDropdown.Item >
                                <NavLink tag={Link} to={`/ConsultarFolio`}  > Consultar folio</NavLink>
                            </NavDropdown.Item>
                        </NavDropdown>
                       
                     
                    </Nav>
                    <Nav>
                        <NavDropdown title={usuario ? usuario : "Usuario no encontrado"} id="collasible-nav-dropdown">
                            <NavDropdown.Item onClick={()=>{
                                window.localStorage.clear();
                                window.location.replace('/');
                            }}>Salir</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                    </Navbar.Collapse>
                    </Container>
            </Navbar>
    }
}