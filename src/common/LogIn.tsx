import React from 'react';
import { Button, Card, Row, Col, Form } from 'react-bootstrap'; 
import axios from 'axios';
import * as url from '../api';
import * as toastr from './Toast';
interface LogInState{
    Usuario: string,
    Contrasena: string
}
export default class LogIn extends React.Component<any,LogInState>{
    constructor(props: any){
        super(props);
        this.state = {
            Usuario: "",
            Contrasena: ""
        };

    }
    public LogIn = () =>{
        if (!this.state.Usuario) {			
			alert("Ingresar usuario para continuar");
			return false;
		}
		if (!this.state.Contrasena) {			
			alert("Ingresar contraseña para continuar");
			return false;
		}
        var localstorage = window.localStorage;
        const headers = {
            'Access-Control-Allow-Origin' : '*',
            'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS'
          };
           axios.post(url.LogIn, {
			Usuario_id: this.state.Usuario,
			Password: this.state.Contrasena
		         },{headers : headers})
			.then(response => response.data)
			.then(data => {
				if (data.Code != "") {
					localstorage.setItem("Token", data.Code);
					localstorage.setItem("Usuario_id", data.OData._UsuarioMo.Usuario_id);
                    localstorage.setItem("NombreUsuario", data.OData._UsuarioMo.Nombre);
                    this.props.history.replace("Inicio");				
				} else {
					alert(data.Message);
                }
			})
			.catch(error => {
				console.log(error);
			})

        
    }
    public render(){
        return <div>
                 <Card style={{ width: '30rem', margin:'0 auto', marginTop:'5em' }} >
                    <Card.Body>
                        <Row>
                        <img
                            src="images/Logo-Brand.png"
                            className="d-inline-block align-top"
                        />
                        </Row>
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12} xl={12} style={{color: 'rgb(14, 126, 195)', fontWeight: 'bolder', fontSize:'18pt', textAlign:'center'}}>
                                Bienvenido
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12} xl={12} style={{color: 'rgb(84, 84, 84)', fontWeight: 'bolder', fontSize:'18pt', textAlign:'center'}}>
                                Inicia sesión en tu cuenta
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                <Form.Group style={{color: 'rgb(84, 84, 84)', fontWeight: 'bolder'}} >
                                    <Form.Label>Usuario</Form.Label>
                                    <Form.Control 
                                         onChange={(evt: any)=>{
                                            this.setState({Usuario: evt.target.value});
                                        }}
                                        style={{color: 'rgb(84, 84, 84)', fontWeight: 'bolder'}}  />
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                <Form.Group style={{color: 'rgb(84, 84, 84)', fontWeight: 'bolder'}} >
                                    <Form.Label>Contraseña</Form.Label>
                                    <Form.Control type="password"
                                        onChange={(evt: any)=>{
                                            this.setState({Contrasena: evt.target.value});
                                        }}
                                        style={{color: 'rgb(84, 84, 84)', fontWeight: 'bolder'}} />
                                </Form.Group>
                            </Col>
                        </Row>
                        <br />
                        <Row>
                            <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                <Form.Group >
                                    <Form.Check 
                                        style={{color: 'rgb(84, 84, 84)', fontWeight: 'bolder'}}
                                        type={"checkbox"}
                                        label={"Recuerdame"}
                                    />
                                </Form.Group>
                            </Col>
                            <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                <Form.Group >
                                    <Form.Label style={{color: 'rgb(14, 126, 195)', fontWeight: 'bolder'}}>¿Olvidaste tu contraseña?</Form.Label>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                <div className="d-grid gap-2">
                                    <Button variant="primary" size="sm" onClick={this.LogIn} >Iniciar sesión</Button>
                                </div>
                            </Col>
                        </Row>
                        <br />
                        <Row>
                            <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                <Form.Group >
                                    <Form.Label style={{color: 'rgb(84,84,84)', fontWeight: 'bolder'}}>¿No tienes una cuenta?</Form.Label>
                                </Form.Group>
                            </Col>
                            <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                <Form.Group  >
                                    <Form.Label style={{color: 'rgb(14, 126, 195)', fontWeight: 'bolder'}}>Registrate ahora</Form.Label>
                                </Form.Group>
                            </Col>
                        </Row>
                    </Card.Body>
            </Card>
             </div>
    }
}