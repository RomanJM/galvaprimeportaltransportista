import React from 'react';
import NavbarTop from './NavbarTop';
import { RouteComponentProps } from 'react-router';
import { Container } from 'react-bootstrap';
export default class Layout extends React.Component<any,any>{
    public render(){
        return <div>
                <NavbarTop />
                <Container>
                    {
                        this.props.children
                    }
                </Container>
            </div>
    }
}