import React from 'react';
import { RouteComponentProps} from 'react-router';
import { Route, Switch, BrowserRouter} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-table/react-table.css';
//import 'mdbreact/dist/css/mdb.css';
import './main.css';
import LogIn from './common/LogIn';
import RecuperaPassword from './common/RecuperaPassword';
import NuevoPassword from './common/NuevoPassword';
import Layout from './common/Layout';
import Home from './pages/Home';
import RegistroEntradaCamiones from './pages/RegistroEntradaCamiones';
import RegistroSalidaCamiones from './pages/RegistroSalidaCamiones';
import DescargaCamiones from './pages/DescargaCamiones';
import CargaCamiones from './pages/CargaCamiones';
import ConsultarFFolio from './pages/ConsultarFolio';
export default class App extends React.Component<any, any>{
  public render(){
    //Local
    const rutaServidor = "/";
    //Produccion
    //const rutaServidor = "/transportistas";
    return <BrowserRouter >
           <Switch>
              <Route exact path={`/`} component={LogIn} />
              <Route path={`/recuperapassword`} component={RecuperaPassword} />
              <Route path={`/NuevoPassword`} component={NuevoPassword} />
              <Layout>
                  <Route path={`/Inicio`} component={Home} />
                  <Route path={`/Entrada`} component={RegistroEntradaCamiones} />
                  <Route path={`/DescargarCamion`} component={DescargaCamiones} />
                  <Route path={`/CargarCamion`} component={CargaCamiones} />
                  <Route path={`/Salida`} component={RegistroSalidaCamiones} />
                  <Route path={`/ConsultarFolio`} component={ConsultarFFolio} />
              </Layout>
           </Switch>
        </BrowserRouter>
  }
}
