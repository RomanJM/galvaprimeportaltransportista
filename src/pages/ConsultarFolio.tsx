import React from "react";
import { Modal, Button, Card, Row, Col, Form } from "react-bootstrap";
import Select from "react-select";
import * as url from "../api";
interface ConsultarFolioProps {}
interface ConsultarFolioState {
  folio: any;
  tipo: any;
}
export default class AgregarChofer extends React.Component<
  ConsultarFolioProps,
  ConsultarFolioState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      folio: "",
      tipo: { value: "entrada", label: "Entrada" },
    };
  }
  public render() {
    const tipo = [
      { value: "salida", label: "Salida" },
      { value: "entrada", label: "Entrada" },
    ];
    return (
      <div>
        <Card>
          <Card.Body>
            <Row>
              <Col xs={12} sm={12} md={2} lg={2} xl={2}></Col>
              <Col xs={12} sm={12} md={3} lg={3} xl={3}>
                <Form.Group>
                  <Form.Label>Ingrese el número de folio</Form.Label>
                  <Form.Control
                    required
                    type="number"
                    value={this.state.folio}
                    onChange={(item: any) => {
                      this.setState({ folio: item.target.value });
                    }}
                  />
                </Form.Group>
              </Col>

              <Col xs={12} sm={12} md={3} lg={3} xl={3}>
                <Form.Group as={Row}>
                  <Form.Label>Seleccione tipo</Form.Label>

                  <Select
                    value={this.state.tipo}
                    options={tipo}
                    defaultValue={{ value: "entrada", label: "Entrada" }}
                    onChange={(item) => {
                      this.setState({ tipo: item });
                    }}
                  />
                </Form.Group>
              </Col>
              <Col xs={12} sm={12} md={3} lg={3} xl={3}>
                <div style={{ margin: 30 }} className="row">
                  {" "}
                  <Form.Group as={Row}>
                    <button
                      className="btn btn-secondary"
                      onClick={() => {
                        if (!this.state.folio) {
                          alert("Ingrese el número de folio para continuar");
                          return false;
                        }
                        var urlFull: any =
                          this.state.tipo.value == "entrada"
                            ? url.ImprimirEntrada + this.state.folio
                            : url.ImprimirSalida + this.state.folio;
                        window.open(urlFull, "_blank");
                      }}
                      style={{ margin: "0 1em" }}
                    >
                      Consultar
                    </button>
                  </Form.Group>
                </div>
              </Col>
              <Col xs={12} sm={12} md={1} lg={1} xl={1}></Col>
            </Row>
          </Card.Body>
        </Card>
      </div>
    );
  }
}
