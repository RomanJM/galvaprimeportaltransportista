import React from "react";
import { Modal, Button, Table } from "react-bootstrap";
import ReactTable from 'react-table';
import * as _ from 'lodash';
interface SeleccionarFolioState {
  show: boolean;
  handleClose: any;
  handleDoubleClick : any;
  foliosPendientes : any[]
}
export default class SeleccionarFolioModal extends React.Component<
  SeleccionarFolioState,
  any
> {
  public render() {
    const columns = [
      {
        Header: "Folio",
        accessor: "DocNum",
        Cell: (row: any) => {
          return <div style={{textAlign : "center"}}>{row.value}</div> ;
        },
        headerStyle: { fontWeight: "bold" },
        minWidth: 40,
      },
      {
        Header: "Tipo",
        accessor: "U_TCarga",
        Cell: (row: any) => {
          return <div style={{textAlign : "center"}}>{row.value}</div> ;
        },
        headerStyle: { fontWeight: "bold" },
        minWidth: 50,
      },
      {
        Header: "Linea Transportista",
        accessor: "U_LTrans",
        Cell: (row: any) => {
          return <div style={{textAlign : "center"}}>{row.value}</div> ;
        },
        headerStyle: { fontWeight: "bold" },
      },
      {
        Header: "Operador",
        accessor: "U_NChofer",
        Cell: (row: any) => {
          return <div style={{textAlign : "center"}}>{row.value}</div> ;
        },
        headerStyle: { fontWeight: "bold" },
      }
    ];

    return (
      <div>
        <Modal size="xl"
      aria-labelledby="contained-modal-title-vcenter"
      centered  show={this.props.show} onHide={this.props.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title style={{fontSize : 17}}>Carga de folio</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div style={{textAlign : "center"}}>
            Doble click para seleccionar un registro
            </div>
          <ReactTable
                data={this.props.foliosPendientes ? this.props.foliosPendientes : []}
                columns={columns}
                showPagination={true}
                showPageSizeOptions={true}
               
                filterable={true}
                className="-striped -highlight detalletabla"
                defaultFilterMethod={(filter, row, column) => {
                    const id = filter.pivotId || filter.id;
                    var text = String(row[id]).toLowerCase();
                    var filtervalue = String(filter.value).toLowerCase();
                    return text !== undefined ? String(text).startsWith(filtervalue) : true;
                }}
                getTdProps={(state : any, rowInfo : any, column : any, instance : any) => {
                  return {
                      onDoubleClick: () => {
                          this.props.handleDoubleClick(rowInfo.original);
                      }
                  };
              }}
                noDataText={"No se encontraron registros"}
                nextText="Siguiente"
                pageText="Pagina"
                rowsText="Registros"
            />       
          </Modal.Body>
          <Modal.Footer>
            <Button size="sm" variant="secondary" onClick={this.props.handleClose}>
              Cerrar
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
