import React from "react";
import { Modal,Button, Card, Row, Col, Form } from 'react-bootstrap';

interface AgregarChoferProps {
  show: boolean;
  handleClose: any;
  handleOk : any;
}
interface AgregarChoferState{
    chofer: any,
   
}
export default class AgregarChofer extends React.Component<
AgregarChoferProps,
AgregarChoferState
> {
    constructor(props: any){
        super(props);
        this.state = {
            chofer: "",
        };

    }
  public render() {
  
    return (
      <div>
        <Modal size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered  show={this.props.show} onHide={this.props.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title style={{fontSize : 17}}>Agregar Chofer</Modal.Title>
          </Modal.Header>
          <Modal.Body>
               <Row>
               <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Nombre Completo</Form.Label>
                            <Col sm="8">
                            <Form.Control value = {this.state.chofer}  onChange={(val: any)=>{
                                this.setState({chofer : val.target.value });
                            }} maxLength={60} size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group>
                    </Col>
                                    
              </Row>
             
          </Modal.Body>
          <Modal.Footer>
          <Button size="sm" variant="primary" onClick={()=>{           
                if(!this.state.chofer){
                 alert("Ingrese el nombre completo del chofer");
                 return false;
                }
                this.props.handleOk(this.state.chofer);
          }}>
              Guardar
            </Button>
            <Button size="sm" variant="secondary" onClick={this.props.handleClose}>
              Cerrar
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
