import React from "react";
import { Modal,Button, Card, Row, Col, Form } from 'react-bootstrap';

interface AgregarPlacasTractorProps {
  show: boolean;
  handleClose: any;
  handleOk : any;
}
interface AgregarPlacasTractorState{
    PlacasTractor: any,
   
}
export default class AgregarPlacasTractor extends React.Component<
AgregarPlacasTractorProps,
AgregarPlacasTractorState
> {
    constructor(props: any){
        super(props);
        this.state = {
            PlacasTractor: "",
        };

    }
  public render() {
  
    return (
      <div>
        <Modal size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered  show={this.props.show} onHide={this.props.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title style={{fontSize : 17}}>Agregar Placa de tractor</Modal.Title>
          </Modal.Header>
          <Modal.Body>
               <Row>
               <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Numero de Placa</Form.Label>
                            <Col sm="8">
                            <Form.Control value = {this.state.PlacasTractor}  onChange={(val: any)=>{
                                this.setState({PlacasTractor : val.target.value });
                            }} maxLength={60} size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group>
                    </Col>
                                    
              </Row>
             
          </Modal.Body>
          <Modal.Footer>
          <Button size="sm" variant="primary" onClick={()=>{           
                if(!this.state.PlacasTractor){
                 alert("Ingrese el numero de placa");
                 return false;
                }
                this.props.handleOk(this.state.PlacasTractor);
          }}>
              Guardar
            </Button>
            <Button size="sm" variant="secondary" onClick={this.props.handleClose}>
              Cerrar
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
