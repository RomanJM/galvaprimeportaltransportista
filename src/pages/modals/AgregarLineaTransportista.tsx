import React from "react";
import { Modal,Button, Card, Row, Col, Form } from 'react-bootstrap';

interface AgregarLineaTransportistaProps {
  show: boolean;
  handleClose: any;
  handleOk : any;
}
interface AgregarLineaTransportistaState{
    clave: any,
    nom_trans : any
   
}
export default class AgregarLineaTransportista extends React.Component<
AgregarLineaTransportistaProps,
AgregarLineaTransportistaState
> {
    constructor(props: any){
        super(props);
        this.state = {
            clave: "",
            nom_trans : ""
            
        };

    }
  public render() {
  
    return (
      <div>
        <Modal size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered  show={this.props.show} onHide={this.props.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title style={{fontSize : 17}}>Agregar linea transportista</Modal.Title>
          </Modal.Header>
          <Modal.Body>
               <Row>
               <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Clave Proveedor</Form.Label>
                            <Col sm="8">
                            <Form.Control value = {this.state.clave}  onChange={(val: any)=>{
                                this.setState({clave : val.target.value });
                            }} maxLength={10} size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group>
                    </Col>
                                    
              </Row>
              <Row>             
                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Nombre Transportista</Form.Label>
                            <Col sm="8">
                            <Form.Control value = {this.state.nom_trans}  onChange={(val: any)=>{
                                this.setState({nom_trans : val.target.value });
                            }} maxLength={60}  required size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group>
                    </Col>           
              </Row>
             
          </Modal.Body>
          <Modal.Footer>
          <Button size="sm" variant="primary" onClick={()=>{
              console.log(this.state.nom_trans);
                if(!this.state.nom_trans){
                 alert("Ingrese el nombre del transportista");
                 return false;
                }
             
                this.props.handleOk(!this.state.clave ? "" : this.state.clave, this.state.nom_trans);
                this.setState({ clave: "",nom_trans : ""});
          }}>
              Guardar
            </Button>
            <Button size="sm" variant="secondary" onClick={()=>{
                  this.setState({ clave: "",nom_trans : ""});
             this.props.handleClose()
            }}>
              Cerrar
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
