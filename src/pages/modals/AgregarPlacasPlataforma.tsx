import React from "react";
import { Modal,Button, Card, Row, Col, Form } from 'react-bootstrap';

interface AgregarPlacasPlataformaProps {
  show: boolean;
  handleClose: any;
  handleOk : any;
}
interface AgregarPlacasPlataformaState{
    PlacasPlataforma: any,
   
}
export default class AgregarPlacasPlataforma extends React.Component<
AgregarPlacasPlataformaProps,
AgregarPlacasPlataformaState
> {
    constructor(props: any){
        super(props);
        this.state = {
            PlacasPlataforma: "",
        };

    }
  public render() {
  
    return (
      <div>
        <Modal size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered  show={this.props.show} onHide={this.props.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title style={{fontSize : 17}}>Agregar Placa de Plataforma</Modal.Title>
          </Modal.Header>
          <Modal.Body>
               <Row>
               <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Numero de Placa</Form.Label>
                            <Col sm="8">
                            <Form.Control value = {this.state.PlacasPlataforma}  onChange={(val: any)=>{
                                this.setState({PlacasPlataforma : val.target.value });
                            }} maxLength={60} size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group>
                    </Col>
                                    
              </Row>
             
          </Modal.Body>
          <Modal.Footer>
          <Button size="sm" variant="primary" onClick={()=>{           
                if(!this.state.PlacasPlataforma){
                 alert("Ingrese el numero de placa");
                 return false;
                }
                this.props.handleOk(this.state.PlacasPlataforma);
          }}>
              Guardar
            </Button>
            <Button size="sm" variant="secondary" onClick={this.props.handleClose}>
              Cerrar
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
