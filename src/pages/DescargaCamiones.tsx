import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store/index';
import { Button, Card, Row, Col, Form, Table } from 'react-bootstrap';
import Select from 'react-select'

import * as DescargaCamionesStore from '../store/DescargaCamionesStore';
type DescargaCamionesProps = DescargaCamionesStore.DescargaCamionesStoreState & typeof DescargaCamionesStore.actionCreators & RouteComponentProps<{}>;

 class DescargaCamiones extends React.Component<DescargaCamionesProps,any>{

    public render(){
        const options = [
            { value: '1', label: 'Opcion 1' },
            { value: '2', label: 'Opcion 2' },
            { value: '3', label: 'Opcion 3' }
          ];
          const tipodescarga = [
            { value: '1', label: 'Compra de materiales' },
            { value: '2', label: 'Devoluciones' },
            { value: '3', label: 'Maquila de clientes' },
            { value: '4', label: 'Maquila propia' }
          ];
        return <div>
            <Card >
            <Card.Header>
                Descarga de camiones               
            </Card.Header>
            <Card.Body>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     DocNum
                     </Form.Label>
                    <Col sm="8">
                    <Form.Control disabled = {true} size="sm" type="text" placeholder="" />
                    </Col>
                </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Fecha
                     </Form.Label>
                    <Col sm="8">
                    <Form.Control disabled = {true} size="sm" type="text" placeholder="" />
                    </Col>
                </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Hora
                     </Form.Label>
                    <Col sm="8">
                    <Form.Control disabled = {true} size="sm" type="text" placeholder="" />
                    </Col>
                </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Folio
                     </Form.Label>
                    <Col sm="8">
                    <Form.Control size="sm" type="text" placeholder="" />
                    </Col>
                </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Tipo descarga
                     </Form.Label>
                    <Col sm="8">
                    <Select 
                                options={tipodescarga} 
                                defaultValue={{value: '', label: 'Seleccionar'}} 
                    />
                    </Col>
                </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Operador
                     </Form.Label>
                    <Col sm="8">
                    <Form.Control size="sm" type="text" placeholder="" />
                    </Col>
                </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="5">
                     Mat. Desc. Corresponde
                     </Form.Label>
                    <Col sm="7">
                    <Form.Control size="sm" type="text" placeholder="" />
                    </Col>
                </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Cantidad Cintas
                     </Form.Label>
                    <Col sm="8">
                    <Form.Control size="sm" type="text" placeholder="0.000" />
                    </Col>
                </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Cantidad Hojas
                     </Form.Label>
                    <Col sm="8">
                    <Form.Control size="sm" type="text" placeholder="0.000" />
                    </Col>
                </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Cantidad Blanks
                     </Form.Label>
                    <Col sm="8">
                    <Form.Control size="sm" type="text" placeholder="0.000" />
                    </Col>
                </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Cantidad Rollos
                     </Form.Label>
                    <Col sm="8">
                    <Form.Control size="sm" type="text" placeholder="0.000" />
                    </Col>
                </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Cantidad Scrap
                     </Form.Label>
                    <Col sm="8">
                    <Form.Control size="sm" type="text" placeholder="0.000" />
                    </Col>
                </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Tipo Transporte
                     </Form.Label>
                    <Col sm="8">
                    <Select 
                                options={options} 
                                defaultValue={{value: '', label: 'Seleccionar'}} 
                    />
                    </Col>
                </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Doc. Entrada
                     </Form.Label>
                    <Col sm="8">
                    <Form.Control size="sm" type="text" placeholder="" />
                    </Col>
                </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Certificado Calidad
                     </Form.Label>
                    <Col sm="8">
                    <Form.Control size="sm" type="text" placeholder="" />
                    </Col>
                </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Comentarios
                     </Form.Label>
                    <Col sm="8">
                    <Form.Control size="sm" type="text" placeholder="" />
                    </Col>
                </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Kilos Reales
                     </Form.Label>
                    <Col sm="8">
                    <Form.Control size="sm" type="text" placeholder="0.000" />
                    </Col>
                </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3">
                    <Form.Label column sm="4">
                     Kilos Estimados
                     </Form.Label>
                    <Col sm="8">
                    <Form.Control size="sm" type="text" placeholder="0.000" />
                    </Col>
                </Form.Group>
                    </Col>
                </Row>

                <div style={{margin:'2em 0', borderBottom:'1px solid #555454'}}></div>
                <Table borderless = {false} responsive size="sm">
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>Clase documento</th>
                        <th>Num. Doc.</th>
                        <th>Cod. SN</th>
                        <th>Nombre SN</th>
                        <th>kilos Estimados</th>
                        <th>kilos Reales</th>
                        <th>Alm. Origen</th>
                        <th>Alm. Destino</th>
                        <th>Num Doc. Proveedor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td>1</td>
                        <td><select style = {{width : "100%"}} name="select" id="select" >
                            <option value="volvo">Volvo</option>
                            <option value="saab">Saab</option>
                            <option value="opel">Opel</option>
                            <option value="audi">Audi</option>
                            </select></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>0.000</td>
                        <td>0.000</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        </tr>
                        <tr>
                        <td>2</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        </tr>
                        <tr>
                        <td>3</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        </tr>
                    </tbody>
                    </Table>
            </Card.Body>
            <Card.Footer >
            <div style={{width:'50%', float:'left', textAlign:'left'}}>
                    <Button size = "sm" variant="success" style={{margin:'0 1em'}}>Crear</Button>
                    <Button size = "sm" variant="danger">Cancelar</Button> 
                </div>
            </Card.Footer>
            </Card>
        </div>
    }
}

export default connect(
    (state: ApplicationState) => state.DescargaCamionesStore, DescargaCamionesStore.actionCreators
)(DescargaCamiones as any);