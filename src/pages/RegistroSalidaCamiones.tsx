import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store/index';
import { Button, Card, Row, Col, Form } from 'react-bootstrap';
import Select from 'react-select';
import * as _ from 'lodash';
import * as url from '../api';
import Loading from '../common/Loading';
import AgregarLineaTransportista from './modals/AgregarLineaTransportista';
import SeleccionarFolioModal from './modals/SeleccionarFolioModal';
import * as RegistroSalidaCamionesStore from '../store/RegistroSalidaCamionesStore';

type RegistroSalidaCamionesProps = RegistroSalidaCamionesStore.RegistroSalidaCamionesStoreState & typeof RegistroSalidaCamionesStore.actionCreators & RouteComponentProps<{}>;
interface RegistroSalidaCamionesState{
    U_Folio: any,
    U_NGuardia: any,
    U_MAP: any,
    U_USE: any,
    U_Remisiones: any,
    U_Traslados : any,
    U_ExitAut: any,
    U_LT: any,
    U_status: any,
    U_Comentarios :  any,
    showModalLinea : any,
    showModalFolio : any
}
 class RegistroSalidaCamiones extends React.Component<RegistroSalidaCamionesProps,RegistroSalidaCamionesState>{
    constructor(props: any){
        super(props);
        this.state = {
            U_MAP: {value: '0', label: 'Seleccionar'},
            U_USE: {value: '0', label: 'Seleccionar'},
            U_ExitAut: {value: '0', label: 'Seleccionar'},
            U_Folio : "",
            U_LT : {value: '0', label: 'Seleccionar'},
            U_NGuardia : {value: '0', label: 'Seleccionar'},
            U_Remisiones : "",
            U_Traslados  :"",
            U_status : {value: 'Pendiente por facturar', label: 'Pendiente por facturar'},
            U_Comentarios : "",
            showModalLinea : false,
            showModalFolio : false
        };

    }
    componentDidMount(){
        this.props.requestInformacionTransportistas();
        this.props.requestFoliosPendientes();
    }
    public ResetState= () =>{
        this.setState({
            U_MAP: {value: '0', label: 'Seleccionar'},
            U_USE: {value: '0', label: 'Seleccionar'},
            U_ExitAut: {value: '0', label: 'Seleccionar'},
            U_Folio : "",
            U_LT : {value: '0', label: 'Seleccionar'},
            U_NGuardia : {value: '0', label: 'Seleccionar'},
            U_Remisiones : "",
            U_Traslados : "",
            U_status : {value: 'Pendiente por facturar', label: 'Pendiente por facturar'},
            U_Comentarios : "",
            showModalLinea : false
        });
    }
    public Enviar= () =>{   
            if(this.state.U_MAP.value == "0" || this.state.U_MAP.value == ""){
                alert('Seleccione una opcion para el campo Material asegurado');
                return false;
            }     
            if(this.state.U_USE.value == "0" || this.state.U_USE.value == ""){
                alert('Seleccione una opcion para el campo Unidad sale enlonada');
                return false;
            } 
            if(this.state.U_ExitAut.value == "0" || this.state.U_ExitAut.value == ""){
                alert('Seleccione una opcion para el campo Autorizacion salida');
                return false;
            } 
            if(this.state.U_LT.value == "Seleccionar"|| this.state.U_LT.label == ""){
                alert('Seleccione una opcion para el campo Linea transportista');
                return false;
            } 
            if(this.state.U_status.value == "0" || this.state.U_status.value == ""){
                alert('Seleccione una opcion para el campo Estatus');
                return false;
            } 
			let u_lt_arr = this.state.U_LT.label.split("-");
			let u_lt=u_lt_arr[0].trim();
			if (u_lt_arr.length>1)
				u_lt=u_lt_arr[1].trim();
            var data = {
                U_Folio: this.state.U_Folio,
                U_NGuardia: window.localStorage.getItem("NombreUsuario"),
                U_MAP: this.state.U_MAP.value ,
                U_USE: this.state.U_USE.value,
                U_Remisiones: this.state.U_Remisiones,
                U_ExitAut: this.state.U_ExitAut.value,
                U_LT: u_lt,
                Remark: this.state.U_LT.value ? this.state.U_LT.value.trim() : "",
                U_status: this.state.U_status.value,
                U_Comments : this.state.U_Comentarios
            };
            this.props.requestGuardarSalida(data);
            this.ResetState();
    }
    public render(){

      var usuario : any =  window.localStorage.getItem("NombreUsuario");
      var choferes : any=  [];
      var placas : any = [];
      var placasplataforma : any= [];
      var selectdefault = {value: '0', label: 'Seleccionar'};
      var transportistas = this.props.transportistas;

        if (this.props.choferes.length > 0){
            choferes.push({value: '0', label: 'Seleccionar'});
            placas.push({value: '0', label: 'Seleccionar'});
            placasplataforma.push({value: '0', label: 'Seleccionar'});
        }

      this.props.choferes.map((x:any)=>{
         var chofer = {"value" : x.OperadorNombre, "label" : x.OperadorNombre};
         choferes.push(chofer);
         if( x.Placa){
            placas.push({"value" : x.Placa, "label" : x.Placa});
         }
         if( x.PlacaPlataforma){
            placasplataforma.push({"value" : x.PlacaPlataforma, "label" : x.PlacaPlataforma});
         }
      });

          const estatus = [
            { value: 'Propio', label: 'Propio' },
            { value: 'Cliente recoge', label: 'Cliente recoge' },                          
            { value: 'Factura recibida', label: 'Factura recibida' },
            { value: 'Pendiente por facturar', label: 'Pendiente por facturar' },     
          ];
          

          const respuestas = [
            { value: 'Si', label: 'Si' },
            { value: 'No', label: 'No' }
          ];
          var identificacion: any = "";
          identificacion = window.localStorage.getItem("Usuario_id");
        return <div>
           {this.props.isloadingSalida ? <Loading/> :
            <Card >
            <Card.Header>
                Registro Salida camiones
                <div style={{width:'50%', float:'right', textAlign:'right'}}>
                    <Button variant="success" onClick={()=>{
                        this.Enviar();
                    }} style={{margin:'0 1em'}}>Crear</Button>
                    {this.props.folio ?  <a className='btn btn-secondary' 
                      href={url.ImprimirSalida+this.props.folio}
                      target={"_blank"}
                     style={{margin:'0 1em'}}>Imprimir ultima salida</a> : null}
                    <Button onClick={()=>{
this.props.resetFolio();
                         this.props.history.push({
                                      pathname: `/Inicio`
                         });
                                 
                    }} variant="danger">Cancelar</Button> 
                </div>
            </Card.Header>
            <Card.Body>
            <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Doc Num.</Form.Label>
                            <Col sm="8">
                            <Form.Control  disabled = {true} size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Fecha</Form.Label>
                            <Col sm="8">
                            <Form.Control  disabled = {true} size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Hora</Form.Label>
                            <Col sm="8">
                            <Form.Control disabled = {true} size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Folio</Form.Label>
                            <Col sm="8">
                            <Form.Control  value={this.state.U_Folio}
                      onClick={() => {
                        this.setState({ showModalFolio: true });
                      }}   size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group>
                    </Col>
                  
                </Row>
                <Row>
                <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Nombre Guardia</Form.Label>
                            <Col sm="8">
                            <Form.Control value={usuario}   disabled = {true} size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group> 
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Mat asegurado plataforma</Form.Label>
                            <Col sm="8">
                            <Select 
                                value = {this.state.U_MAP}
                                options={respuestas} 
                                onChange = {(item : any)=>{                                 
                                    this.setState({U_MAP : item});
                                    }}
                                defaultValue={{value: '0', label: 'Seleccionar'}} 
                            />
                            </Col>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="5">Unidad sale enlonada</Form.Label>
                            <Col sm="7">
                            <Select 
                            value={this.state.U_USE}
                                options={respuestas} 
                                defaultValue={{value: '', label: 'Seleccionar'}} 
                                onChange = {(item)=>{
                                    this.setState({U_USE : item});
                               }}
                            />
                            </Col>
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="5">Comentarios</Form.Label>
                            <Col sm="7">
                            <Form.Control value = {this.state.U_Comentarios}  onChange={(val: any)=>{
                                this.setState({U_Comentarios : val.target.value });
                            }}  />
                            </Col>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Remisiones</Form.Label>
                            <Col sm="8">
                            <Form.Control value = {this.state.U_Remisiones}  onChange={(val: any)=>{
                                this.setState({U_Remisiones : val.target.value });
                            }}    size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Traslados</Form.Label>
                            <Col sm="8">
                            <Form.Control value = {this.state.U_Traslados}  onChange={(val: any)=>{
                                this.setState({U_Traslados : val.target.value });
                            }}    size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group>
                    </Col>
                  
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="5">Autorizacion salida</Form.Label>
                            <Col sm="7">
                            <Select 
                            value={this.state.U_ExitAut}
                                options={respuestas} 
                                defaultValue={{value: '', label: 'Seleccionar'}} 
                                onChange = {(item)=>{
                                    this.setState({U_ExitAut : item});
                               }}
                            />
                            </Col>
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Linea transportista</Form.Label>
                            <Col sm="6">
                            <Select 
                                value = {this.state.U_LT}
                                options={transportistas} 
                                onChange = {(item : any)=>{
                                    this.setState({U_LT : item }); }}                        
                                defaultValue={{value: '0', label: 'Seleccionar'}} 
                            />
                            </Col>
                            <Col sm="2">
                            <label style={{textDecoration : "underline", cursor : "pointer"}}  onClick={()=>{this.setState({showModalLinea : true})}}>Nuevo</label>
                            </Col>
                        </Form.Group>
                        </Form.Group>
                    </Col>
                </Row>
                <br />
                <br />
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="5">Estatus</Form.Label>
                            <Col sm="7">
                            <Select 
                              value={this.state.U_status}
                                options={estatus} 
                                defaultValue={{value: '', label: 'Seleccionar'}} 
                                onChange = {(item)=>{
                                    this.setState({U_status : item});
                               }}
                               
                            />
                            </Col>
                        </Form.Group>
                    </Col>
                </Row>
            </Card.Body>
            </Card>}
            <AgregarLineaTransportista
          show={this.state.showModalLinea}
          handleClose={() => {
            this.setState({ showModalLinea: false });
          }}
          handleOk={(clave: any, nombre : any) => {        
            var trans = {value : clave, label : (clave + " - "+nombre)};
            var existe = _.find(transportistas,(x:any)=>{
                return x.label ==  (clave + " - "+nombre);
            });
            if(existe){
                alert("Ya existe un registro con el mismo nombre y clave, por favor verifique");
                return false;
            }
            this.setState({ showModalLinea: false, U_LT : trans});
            this.props.requestAddTransportista(trans);   
            
          }}
        />
        <SeleccionarFolioModal
        foliosPendientes={this.props.foliosPendientes}
          show={this.state.showModalFolio}
          handleClose={() => {
            this.setState({ showModalFolio: false });
          }} 
          handleDoubleClick={(row: any) => {
              var lt = _.find(transportistas,(x:any)=>{
                  var value = x.label.split('-')[1];
                  if(row.U_LTrans === (value ? value.toString() : value) ){
                         
                  }
                  value = value ? value.toString().trim() : "0xx";
                  
                  return row.U_LTrans ===  value;
              });
             
            this.setState({ U_Folio: row.DocNum, showModalFolio: false, U_Remisiones : row.U_NumRemision,U_Traslados : row.U_NumTraslado , U_LT : lt ? lt :{value: '', label: row.U_LTrans} });
          }}
        />
        </div>
    }
}

export default connect(
    (state: ApplicationState) => state.RegistroSalidaCamionesStore, RegistroSalidaCamionesStore.actionCreators
)(RegistroSalidaCamiones as any);