import React from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { ApplicationState } from "../store/index";
import { Button, Card, Row, Col, Form, Table ,Tab,Tabs} from "react-bootstrap";
import Select from "react-select";
import SeleccionarFolioModal from "./modals/SeleccionarFolioModal";

import * as DescargaCamionesStore from "../store/DescargaCamionesStore";
import { createFalse } from "typescript";
type CargaCamionesProps = DescargaCamionesStore.DescargaCamionesStoreState &
  typeof DescargaCamionesStore.actionCreators &
  RouteComponentProps<{}>;
interface CargaCamionesState {
  showModalFolio: boolean;
  folio: any;
  m_b_e: any;
  m_c_corresponde: any;
  tipo_carga: any;
  tipo_transporte : any;
  doc_remision : any;
  doc_factura : any;
  distribucion : any;
  key : any;
  certificado_calidad : any;
}
class CargaCamiones extends React.Component<
  CargaCamionesProps,
  CargaCamionesState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      showModalFolio: false,
      folio: null,
      m_b_e: { value: "0", label: "Seleccionar" },
      m_c_corresponde: { value: "0", label: "Seleccionar" },
      tipo_carga: { value: "0", label: "Seleccionar" },
      tipo_transporte : { value: "0", label: "Seleccionar" },
      certificado_calidad : { value: "0", label: "Seleccionar" },
      doc_remision :  { value: "0", label: "Seleccionar" },
      doc_factura : { value: "0", label: "Seleccionar" },
      distribucion :  { value: "0", label: "Seleccionar" },
      key : "remisiones"
    };
  }
  public render() {
    const options = [
      { value: "1", label: "Opcion 1" },
      { value: "2", label: "Opcion 2" },
      { value: "3", label: "Opcion 3" },
    ];
    const tipodescarga = [
      { value: "1", label: "Compra de materiales" },
      { value: "2", label: "Devoluciones" },
      { value: "3", label: "Maquila de clientes" },
      { value: "4", label: "Maquila propia" },
    ];
    const tipoTransporte = [
        { value: "Cliente Recoge", label: "Cliente Recoge" },
        { value: "Propio", label: "Propio" },
        { value: "Proveedor Envía", label: "Proveedor Envía" },
        { value: "SubContratado", label: "SubContratado" },
      ];
    const respuestas = [
      { value: "Si", label: "Si" },
      { value: "No", label: "No" },
    ];
    const tipoCarga = [
      { value: "Embarque a Cliente", label: "Embarque a Cliente" },
      { value: "Maquila Propia", label: "Maquila Propia" },
      { value: "Traslado Almacén", label: "Traslado Almacén" }
    ];
    const distribucion = [
        { value: "Directa", label: "Directa" },
        { value: "Reparto", label: "Reparto" },
      ];
    return (
      <div>
        <Card>
          <Card.Header>Registro de Carga</Card.Header>
          <Card.Body>
            <Row>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="4">
                    DocNum
                  </Form.Label>
                  <Col sm="8">
                    <Form.Control
                      disabled={true}
                      size="sm"
                      type="text"
                      placeholder=""
                    />
                  </Col>
                </Form.Group>
              </Col>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="4">
                    Fecha
                  </Form.Label>
                  <Col sm="8">
                    <Form.Control
                      disabled={true}
                      size="sm"
                      type="text"
                      placeholder=""
                    />
                  </Col>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="4">
                    Hora
                  </Form.Label>
                  <Col sm="8">
                    <Form.Control
                      disabled={true}
                      size="sm"
                      type="text"
                      placeholder=""
                    />
                  </Col>
                </Form.Group>
              </Col>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="4">
                    Folio
                  </Form.Label>
                  <Col sm="8">
                    <Form.Control
                      value={this.state.folio}
                      onClick={() => {
                        this.setState({ showModalFolio: true });
                      }}
                      size="sm"
                      type="text"
                      placeholder=""
                    />
                  </Col>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="4">
                    Carga Total Viaje
                  </Form.Label>
                  <Col sm="8">
                    <Form.Control
                      disabled={false}
                      size="sm"
                      type="text"
                      placeholder=""
                    />
                  </Col>
                </Form.Group>
              </Col>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="4">
                    Personal de Embarque
                  </Form.Label>
                  <Col sm="8">
                    <Form.Control
                     
                      size="sm"
                      type="text"
                      placeholder=""
                    />
                  </Col>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="5">
                    Mat. Bien Empacado
                  </Form.Label>
                  <Col sm="7">
                    <Select
                      value={this.state.m_b_e}
                      options={respuestas}
                      defaultValue={{ value: "", label: "Seleccionar" }}
                      onChange={(item) => {
                        this.setState({ m_b_e: item });
                      }}
                    />
                  </Col>
                </Form.Group>
              </Col>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="5">
                    Mat. Carg. Corresponde
                  </Form.Label>
                  <Col sm="7">
                    <Select
                      value={this.state.m_c_corresponde}
                      options={respuestas}
                      onChange={(item) => {
                        this.setState({ m_c_corresponde: item });
                      }}
                      defaultValue={{ value: "0", label: "Seleccionar" }}
                    />
                  </Col>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="4">
                    Cantidad Hojas
                  </Form.Label>
                  <Col sm="8">
                    <Form.Control
                      disabled={false}
                      size="sm"
                      type="text"
                      placeholder=""
                    />
                  </Col>
                </Form.Group>
              </Col>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="4">
                    Cantiad Cintas
                  </Form.Label>
                  <Col sm="8">
                    <Form.Control
                      disabled={false}
                      size="sm"
                      type="text"
                      placeholder=""
                    />
                  </Col>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="4">
                    Cantidad Blanks
                  </Form.Label>
                  <Col sm="8">
                    <Form.Control
                      disabled={false}
                      size="sm"
                      type="text"
                      placeholder=""
                    />
                  </Col>
                </Form.Group>
              </Col>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="4">
                    Cantiad Rollos
                  </Form.Label>
                  <Col sm="8">
                    <Form.Control
                      disabled={false}
                      size="sm"
                      type="text"
                      placeholder=""
                    />
                  </Col>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="4">
                    Comentarios
                  </Form.Label>
                  <Col sm="8">
                    <Form.Control
                      disabled={false}
                      size="sm"
                      type="text"
                      placeholder=""
                    />
                  </Col>
                </Form.Group>
              </Col>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="5">
                    Tipo Carga
                  </Form.Label>
                  <Col sm="7">
                    <Select
                      value={this.state.tipo_carga}
                      options={tipoCarga}
                      onChange={(item) => {
                        this.setState({ tipo_carga: item });
                      }}
                      defaultValue={{ value: "0", label: "Seleccionar" }}
                    />
                  </Col>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="5">
                    Tipo Transporte
                  </Form.Label>
                  <Col sm="7">
                    <Select
                      value={this.state.tipo_transporte}
                      options={tipoTransporte}
                      defaultValue={{ value: "", label: "Seleccionar" }}
                      onChange={(item) => {
                        this.setState({ tipo_transporte: item });
                      }}
                    />
                  </Col>
                </Form.Group>
              </Col>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="5">
                    Doc. Factura
                  </Form.Label>
                  <Col sm="7">
                    <Select
                      value={this.state.doc_factura}
                      options={respuestas}
                      onChange={(item) => {
                        this.setState({ doc_factura: item });
                      }}
                      defaultValue={{ value: "0", label: "Seleccionar" }}
                    />
                  </Col>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="5">
                    Doc. Remisión
                  </Form.Label>
                  <Col sm="7">
                    <Select
                      value={this.state.doc_remision}
                      options={respuestas}
                      defaultValue={{ value: "", label: "Seleccionar" }}
                      onChange={(item) => {
                        this.setState({ doc_remision: item });
                      }}
                    />
                  </Col>
                </Form.Group>
              </Col>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="5">
                    Certificado Calidad
                  </Form.Label>
                  <Col sm="7">
                    <Select
                      value={this.state.certificado_calidad}
                      options={respuestas}
                      onChange={(item) => {
                        this.setState({ certificado_calidad: item });
                      }}
                      defaultValue={{ value: "0", label: "Seleccionar" }}
                    />
                  </Col>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="4">
                    Capacidad de Carga
                  </Form.Label>
                  <Col sm="8">
                    <Form.Control
                      disabled={false}
                      size="sm"
                      type="text"
                      placeholder=""
                    />
                  </Col>
                </Form.Group>
              </Col>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="5">
                    Distribución
                  </Form.Label>
                  <Col sm="7">
                    <Select
                      value={this.state.distribucion}
                      options={distribucion}
                      onChange={(item) => {
                        this.setState({ distribucion: item });
                      }}
                      defaultValue={{ value: "0", label: "Seleccionar" }}
                    />
                  </Col>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="4">
                    Numero de Repartos
                  </Form.Label>
                  <Col sm="8">
                    <Form.Control
                      disabled={false}
                      size="sm"
                      type="text"
                      placeholder=""
                    />
                  </Col>
                </Form.Group>
              </Col>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm="4">
                    Cantidad de Tarimas
                  </Form.Label>
                  <Col sm="8">
                    <Form.Control
                      disabled={false}
                      size="sm"
                      type="text"
                      placeholder=""
                    />
                  </Col>
                </Form.Group>
              </Col>
            </Row>
            <div
              style={{ margin: "2em 0", borderBottom: "1px solid #555454" }}
            ></div>
            <Tabs
              id="controlled-tab-example"
              activeKey={this.state.key}
              onSelect={(k)=>{
                this.setState({key : k});
              }}
              className="mb-3"
            >
              <Tab eventKey="remisiones" title="Remisiones">
              <Table borderless={false} responsive size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Num. Remision</th>
                  <th>Cod. Cliente</th>
                  <th>Nombre Cliente</th>
                  <th>Kilos</th>
                  <th>Tipo Destino</th>
                  <th>Destino</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td> 
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><select style={{ width: "100%" }} name="select" id="select">
                      <option value="volvo">Foraneo</option>
                      <option value="saab">Local</option>
                      <option value="opel">Foraneo Corto</option>
                      <option value="audi">Foraneo Largo</option>
                    </select></td>
                  <td></td>
       
                </tr>
                <tr>
                  <td>2</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>      
                </tr>
                <tr>
                  <td>3</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>            
                </tr>
              </tbody>
            </Table>
              </Tab>
              <Tab eventKey="traslados" title="Traslados">
              <Table borderless={false} responsive size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Num. Traslado</th>
                  <th>Almacen Origen</th>
                  <th>Almacen Destino</th>
                  <th>Comentarios</th>
                  <th>Kilos</th>
                  <th>Tipo Destino</th>
                  <th>Destino</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td> 
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><select style={{ width: "100%" }} name="select" id="select">
                      <option value="volvo">Foraneo</option>
                      <option value="saab">Local</option>
                      <option value="opel">Foraneo Corto</option>
                      <option value="audi">Foraneo Largo</option>
                    </select></td>
                  <td></td>
       
                </tr>
                <tr>
                  <td>2</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>   
                  <td></td>  
                </tr>
                <tr>
                  <td>3</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>  
                  <td></td>          
                </tr>
              </tbody>
            </Table>
              </Tab>
       
            </Tabs>
         
          </Card.Body>
          <Card.Footer>
            <div style={{ width: "50%", float: "left", textAlign: "left" }}>
              <Button size="sm" variant="success" style={{ margin: "0 1em" }}>
                OK
              </Button>
              <Button onClick={()=>{
                         this.props.history.push({
                                      pathname: `/Inicio`
                         });
                                 
                    }} size="sm" variant="danger">
                Cancelar
              </Button>
            </div>
          </Card.Footer>
        </Card>
        <SeleccionarFolioModal
           foliosPendientes={[]}
          show={this.state.showModalFolio}
          handleClose={() => {
            this.setState({ showModalFolio: false });
          }}
          handleDoubleClick={(row: any) => {
            this.setState({ folio: row.folio, showModalFolio: false });
          }}
        />
      </div>
    );
  }
}

export default connect(
  (state: ApplicationState) => state.DescargaCamionesStore,
  DescargaCamionesStore.actionCreators
)(CargaCamiones as any);
