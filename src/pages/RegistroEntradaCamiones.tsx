import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store/index';
import { Button, Card, Row, Col, Form } from 'react-bootstrap';
import Select from 'react-select'
import * as _ from 'lodash';
import Loading from '../common/Loading';
import AgregarLineaTransportista from './modals/AgregarLineaTransportista'; 
import AgregarChofer from './modals/AgregarChofer';
import AgregarPlacasTractor from './modals/AgregarPlacasTractor';
import AgregarPlacasPlataforma from './modals/AgregarPlacasPlataforma';
import * as url from '../api';
import * as RegistroEntradaCamionesStore from '../store/RegistroEntradaCamionesStore';

type RegistroEntradaCamionesProps = RegistroEntradaCamionesStore.RegistroEntradaCamionesStoreState & typeof RegistroEntradaCamionesStore.actionCreators & RouteComponentProps<{}>;
interface RegistroEntradaCamionesState{
    U_NGuardia: any,
            U_LTrans: any,
            U_NChofer: any,
            U_PTractor: any,
            U_PPlataforma: any,
            U_ID: any,
            U_TPG: any,
            U_OQP: any,
            U_ESPC: any,
            U_AC: any,
            U_PPBC: any,
            U_GPS: any,
            U_CBE: any,
            U_Comentarios: any,
            U_TipoDoc: any,
            U_PPlataforma2: any,
            U_Celular: any,
            showModalLinea : any,
            showModalChofer : any,
            showModalPlacasTractor : any,
            showModalPlacasPlataforma : any
}       
 class RegistroEntradaCamiones extends React.Component<RegistroEntradaCamionesProps,RegistroEntradaCamionesState>{

    constructor(props: any){
        super(props);
        this.state = {
            U_NGuardia: {value: '0', label: 'Seleccionar'},
            U_LTrans: {value: '0', label: 'Seleccionar'},
            U_NChofer: {value: '0', label: 'Seleccionar'},
            U_PTractor: {value: '0', label: 'Seleccionar'},
            U_PPlataforma: {value: '0', label: 'Seleccionar'},
            U_ID: "",
            U_TPG: {value: '0', label: 'Seleccionar'},
            U_OQP: {value: '0', label: 'Seleccionar'},
            U_ESPC: {value: '0', label: 'Seleccionar'},
            U_AC: {value: '0', label: 'Seleccionar'},
            U_PPBC: {value: '0', label: 'Seleccionar'},
            U_GPS: {value: '0', label: 'Seleccionar'},
            U_CBE: {value: '0', label: 'Seleccionar'},
            U_Comentarios: "",
            U_TipoDoc: {value: '0', label: 'Seleccionar'},
            U_PPlataforma2: "",
            U_Celular: "",
            showModalLinea : false,
            showModalChofer : false,
            showModalPlacasTractor : false,
            showModalPlacasPlataforma : false
        };

    }
    componentDidMount(){
        this.props.requestInformacionTransportistas();
    }
    public ResetState= () =>{
        this.setState({
            U_NGuardia: {value: '0', label: 'Seleccionar'},
            U_LTrans: {value: '0', label: 'Seleccionar'},
            U_NChofer: {value: '0', label: 'Seleccionar'},
            U_PTractor: {value: '0', label: 'Seleccionar'},
            U_PPlataforma: {value: '0', label: 'Seleccionar'},
            U_ID: "",
            U_TPG: {value: '0', label: 'Seleccionar'},
            U_OQP: {value: '0', label: 'Seleccionar'},
            U_ESPC: {value: '0', label: 'Seleccionar'},
            U_AC:  {value: '0', label: 'Seleccionar'},
            U_PPBC: {value: '0', label: 'Seleccionar'},
            U_GPS: {value: '0', label: 'Seleccionar'},
            U_CBE: {value: '0', label: 'Seleccionar'},
            U_Comentarios: "",
            U_TipoDoc: {value: '0', label: 'Seleccionar'},
            U_PPlataforma2: "",
            U_Celular: "",
            showModalLinea : false,
            showModalChofer : false,
            showModalPlacasTractor : false,
            showModalPlacasPlataforma : false,
        });
    }
    public Enviar= () =>{
            if(this.state.U_LTrans.label == "Seleccionar" || this.state.U_LTrans.label == ""){
                alert('Seleccione el transportista');
                return false;
            }
            if(this.state.U_NChofer.value == "0" || this.state.U_NChofer.value == ""){
                alert('Seleccione el chofer');
                return false;
            }
            if(this.state.U_PTractor.value == "0" || this.state.U_PTractor.value == ""){
                alert('Seleccione las placas para continuar');
                return false;
            }
            if(this.state.U_PPlataforma.value == "0" || this.state.U_PPlataforma.value == ""){
                alert('Seleccione las placas plataforma para continuar');
                return false;
            }
            if(this.state.U_TPG.value == "0" || this.state.U_TPG.value == ""){
                alert('Seleccione transporte padron galvaprime para continuar');
                return false;
            }
            if(this.state.U_OQP.value == "0" || this.state.U_OQP.value == ""){
                alert('Seleccione operador equipo proteccion para continuar');
                return false;
            }
            if(this.state.U_ESPC.value == "0" || this.state.U_ESPC.value == ""){
                alert('Seleccione Esq Sujeto y Protección Carga para continuar');
                return false;
            }
            if(this.state.U_PPBC.value == "0" || this.state.U_PPBC.value == ""){
                alert('Seleccione Piso plataforma, buenas condiciones para continuar');
                return false;
            }
            if(this.state.U_GPS.value == "0" || this.state.U_GPS.value == ""){
                alert('Seleccione GPS para continuar');
                return false;
            }
            if(this.state.U_CBE.value == "0" || this.state.U_CBE.value == ""){
                alert('Seleccione Cortinas buen estado para continuar');
                return false;
            }
            if(this.state.U_TipoDoc.value == "0" || this.state.U_TipoDoc.value == ""){
                alert('Seleccione tipo documento para continuar');
                return false;
            }
           
            if(this.state.U_AC.value=="0"||this.state.U_AC.value==""){
                alert('Ingrese la apariencia del camion para continuar');
                return false;
            }
            var data = {
                "U_NGuardia": window.localStorage.getItem("NombreUsuario"),
                "U_LTrans": this.state.U_LTrans.label.split("-")[1].trim(),
                "Remark": this.state.U_LTrans.value ? this.state.U_LTrans.value.trim() : "",
                "U_NChofer": this.state.U_NChofer.value,
                "U_PTractor": this.state.U_PTractor.value,
                "U_PPlataforma": this.state.U_PPlataforma.value,
                "U_ID": this.state.U_ID ? this.state.U_ID : "",
                "U_TPG": this.state.U_TPG.value,
                "U_OQP": this.state.U_OQP.value,
                "U_ESPC": this.state.U_ESPC.value,
                "U_AC": this.state.U_AC.value,
                "U_PPBC": this.state.U_PPBC.value,
                "U_GPS": this.state.U_GPS.value,
                "U_CBE": this.state.U_CBE.value,
                "U_Comentarios": this.state.U_Comentarios,
                "U_TipoDoc": this.state.U_TipoDoc.value,
                "U_PPlataforma2": this.state.U_PPlataforma2 ? this.state.U_PPlataforma2 : "",
                "U_Celular": this.state.U_Celular ? this.state.U_Celular : ""
            };
            this.props.requestGuardarEntrada(data);
            this.ResetState();
    }
    public render(){
      var usuario : any =  window.localStorage.getItem("NombreUsuario");
      var choferes : any=  [];
      var placas : any = [];
      var placasplataforma : any= [];
      var selectdefault = {value: '0', label: 'Seleccionar'};
      var transportistas = this.props.transportistas;
        if (this.props.choferes.length > 0){
            choferes.push({value: '0', label: 'Seleccionar'});
            placas.push({value: '0', label: 'Seleccionar'});
            placasplataforma.push({value: '0', label: 'Seleccionar'});
        }

      this.props.choferes.map((x:any)=>{
         var chofer = {"value" : x.OperadorNombre, "label" : x.OperadorNombre};
         choferes.push(chofer);
         if( x.Placa){
            placas.push({"value" : x.Placa, "label" : x.Placa});
         }
         if( x.PlacaPlataforma){
            placasplataforma.push({"value" : x.PlacaPlataforma, "label" : x.PlacaPlataforma});
         }
      });
          const tipo = [
            { value: 'Carga', label: 'Carga' },
            { value: 'Descarga', label: 'Descarga' }
          ];
          const respuestas = [
            { value: 'Si', label: 'Si' },
            { value: 'No', label: 'No' }
          ];
          var identificacion: any = "";
          identificacion = window.localStorage.getItem("Usuario_id");
        return <div>
            {this.props.isloadingEntrada ? <Loading/>:<Card >
            <Card.Header>
                Registro entrada camiones
                <div style={{width:'50%', float:'right', textAlign:'right'}}>
                    <Button variant="success" onClick={()=>{
                        this.Enviar();
                    }} style={{margin:'0 1em'}}>Guardar</Button>
                    {this.props.folio ?  <a className='btn btn-secondary' 
                      href={url.ImprimirEntrada+this.props.folio}
                      target={"_blank"}
                     style={{margin:'0 1em'}}>Imprimir ultima entrada</a> : null}
                    <Button onClick={()=>{
                         //window.history.go(1);
                         this.props.resetFolio();
                         this.props.history.push({
                                      pathname: `/Inicio`
                         });
                                 
                    }} variant="danger">Cancelar</Button> 
                </div>
            </Card.Header>
            <Card.Body>
            <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Folio</Form.Label>
                            <Col sm="8">
                            <Form.Control  disabled = {true} size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Fecha</Form.Label>
                            <Col sm="8">
                            <Form.Control  disabled = {true} size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Hora</Form.Label>
                            <Col sm="8">
                            <Form.Control  disabled = {true} size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Nombre Guardia</Form.Label>
                            <Col sm="8">
                            <Form.Control value={usuario}   disabled = {true} size="sm" type="text" placeholder="" />
                            </Col>
                        </Form.Group>                   
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Linea transportista</Form.Label>
                            <Col sm="6">
                            <Select 
                                value = {this.state.U_LTrans}
                                options={transportistas} 
                                onChange = {(item : any)=>{
                                    this.setState({U_LTrans : item , U_NChofer : selectdefault,U_PPlataforma : selectdefault, U_PTractor : selectdefault });
                                    var label = item.label.split('-')[0];
                                        this.props.requestFiltrarChofer(label);                                  
                                    }}
                                defaultValue={{value: '0', label: 'Seleccionar'}} 
                            />
                            </Col>
                            <Col sm="2">
                            <label style={{textDecoration : "underline", cursor : "pointer"}}  onClick={()=>{this.setState({showModalLinea : true})}}>Nuevo</label>
                            </Col>
                           
                        </Form.Group>
                        
                        
                      
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Nombre Chofer</Form.Label>
                            <Col sm="6">
                            <Select 
                                value = {this.state.U_NChofer}
                                options={choferes} 
                                onChange = {(item : any)=>{
                                    this.setState({U_NChofer : item });
                                    var chofer = _.find(this.props.choferes,(x: any)=>{
                                        return x.OperadorNombre == item.value;
                                    });                             
                                    if(chofer != null){
                                       var placa : any = _.find(placas,(x:any)=>{
                                           return x.value == chofer.Placa;
                                       });
                                       var placaplata : any = _.find(placasplataforma,(x:any)=>{
                                        return x.value == chofer.PlacaPlataforma;
                                    });

                                       this.setState({U_PTractor :placa  ? placa : selectdefault,U_PPlataforma : placaplata ? placaplata : selectdefault,U_ID : chofer ? chofer.OperadorTipoID : ""});
                                    }

                                    }}
                                defaultValue={{value: '0', label: 'Seleccionar'}} 
                            />
                            </Col>
                            <Col sm="2">
                            <label style={{textDecoration : "underline", cursor : "pointer"}}  onClick={()=>{this.setState({showModalChofer : true})}}>Nuevo</label>
                            </Col>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Placas tractor</Form.Label>
                            <Col sm="6">
                            <Select 
                                value={this.state.U_PTractor}
                                options={placas} 
                                defaultValue={{value: '0', label: 'Seleccionar'}} 
                                onChange = {(item : any)=>{
                                    this.setState({U_PTractor : item});
                                }}
                            />
                            </Col>
                            <Col sm="2">
                            <label style={{textDecoration : "underline", cursor : "pointer"}}  onClick={()=>{this.setState({showModalPlacasTractor : true})}}>Nuevo</label>
                            </Col>
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Placas plataforma</Form.Label>
                            <Col sm="6">
                            <Select 
                                  value={this.state.U_PPlataforma}
                                options={placasplataforma} 
                                defaultValue={{value: '0', label: 'Seleccionar'}} 
                                onChange = {(item : any)=>{
                                    this.setState({U_PPlataforma : item});
                                }}
                            />
                            </Col>
                            <Col sm="2">
                            <label style={{textDecoration : "underline", cursor : "pointer"}}  onClick={()=>{this.setState({showModalPlacasPlataforma : true})}}>Nuevo</label>
                            </Col>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group  as={Row} className="mb-3" >
                            <Form.Label column sm="4">Identificación</Form.Label>
                            <Col sm="8">                           
                            <Form.Control  value = {this.state.U_ID}  onChange={(val: any)=>{
                                this.setState({U_ID : val.target.value });
                            }}   size="sm" type="text"  />
                            </Col>
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="5">Trans Padrón Galvaprime</Form.Label>
                            <Col sm="7">
                            <Select 
                            value={this.state.U_TPG}
                                options={respuestas} 
                                defaultValue={{value: '', label: 'Seleccionar'}} 
                                onChange = {(item)=>{
                                    this.setState({U_TPG : item});
                               }}
                            />
                            </Col>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="5">Operador Equipo Protección</Form.Label>
                            <Col sm="7">
                            <Select 
                            value={this.state.U_OQP}
                                options={respuestas} 
                                defaultValue={{value: '0', label: 'Seleccionar'}} 
                                onChange = {(item)=>{
                                    this.setState({U_OQP : item});
                               }}
                            />
                            </Col>
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="5">Esq Sujeto y Protección Carga</Form.Label>
                            <Col sm="7">
                            <Select 
                             value={this.state.U_ESPC}
                                options={respuestas} 
                                defaultValue={{value: '0', label: 'Seleccionar'}} 
                                onChange = {(item)=>{
                                    this.setState({U_ESPC : item});
                               }}
                            />
                            </Col>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="4">Apariencia del camión</Form.Label>
                            <Col sm="8">
							<Select 
                              value={this.state.U_AC}
                                options={respuestas} 
                                defaultValue={{value: '', label: 'Seleccionar'}} 
                                onChange = {(item)=>{
                                    this.setState({U_AC : item});
                               }}
                            />
                            </Col>
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="5">Piso plataforma, buenas condiciones</Form.Label>
                            <Col sm="7">
                            <Select 
                              value={this.state.U_PPBC}
                                options={respuestas} 
                                defaultValue={{value: '', label: 'Seleccionar'}} 
                                onChange = {(item)=>{
                                    this.setState({U_PPBC : item});
                               }}
                            />
                            </Col>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="5">Tiene GPS</Form.Label>
                            <Col sm="7">
                            <Select 
                            value={this.state.U_GPS}
                                options={respuestas} 
                                defaultValue={{value: '', label: 'Seleccionar'}} 
                                onChange = {(item)=>{
                                    this.setState({U_GPS : item});
                               }}
                            />
                            </Col>
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <Form.Group as={Row} className="mb-3" >
                            <Form.Label column sm="5">Cortinas buen estado</Form.Label>
                            <Col sm="7">
                            <Select 
                            value = {this.state.U_CBE}
                                options={respuestas} 
                                onChange = {(item)=>{
                                    this.setState({U_CBE : item});
                               }}
                                defaultValue={{value: '0', label: 'Seleccionar'}} 
                            />
                            </Col>
                        </Form.Group>
                    </Col>
                </Row>
                <div style={{margin:'2em 0', borderBottom:'1px solid #555454'}}></div>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Comentarios</Form.Label>
                            <Form.Control value = {this.state.U_Comentarios}  onChange={(val: any)=>{
                                this.setState({U_Comentarios : val.target.value });
                            }}  />
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Tipo</Form.Label>
                            <Select 
                                options={tipo} 
                                value={this.state.U_TipoDoc}
                                defaultValue={{value: '0', label: 'Seleccionar'}} 
                                onChange = {(item)=>{
                                     this.setState({U_TipoDoc : item});
                                }}
                            />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Placas platafforma 2</Form.Label>
                            <Form.Control value = {this.state.U_PPlataforma2} onChange={(val: any)=>{
                                this.setState({U_PPlataforma2 : val.target.value });
                            }}  />
                        </Form.Group>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <Form.Group >
                            <Form.Label>Numero Celular</Form.Label>
                            <Form.Control value = {this.state.U_Celular} onChange={(val: any)=>{
                                this.setState({U_Celular : val.target.value });
                            }}  />
                        </Form.Group>
                    </Col>
                </Row>
            </Card.Body>
            </Card> }
            
            <AgregarLineaTransportista
          show={this.state.showModalLinea}
          handleClose={() => {
            this.setState({ showModalLinea: false });
          }}
          handleOk={(clave: any, nombre : any) => {        
            var trans = {value : clave, label : (clave + " - "+nombre)};
           
            var existe = _.find(transportistas,(x:any)=>{
                
                return x.label == (clave + " - "+nombre);
            });
            if(existe){
                alert("Ya existe un registro con el mismo nombre y clave, por favor verifique");
                return false;
            }
            this.setState({ showModalLinea: false, U_LTrans : trans});
            this.props.requestAddTransportista(trans);
            this.props.requestFiltrarChofer(nombre);     
            
          }}
        />
         <AgregarChofer
          show={this.state.showModalChofer}
          handleClose={() => {
            this.setState({ showModalChofer: false });
          }}
          handleOk={( nombre : any) => {
            var chofer = {value : nombre, label : nombre};
            var choferProps = {OperadorNombre : nombre};
            var existe = _.find(choferes,(x:any)=>{
                return x.label == nombre;
            });
            if(existe){
                alert("Ya existe un registro con el mismo nombre, por favor verifique");
                return false;
            }
            
             this.setState({ showModalChofer: false, U_NChofer : chofer});
             this.props.requestAddChofer(choferProps);
             var chof = _.find(this.props.choferes,(x: any)=>{
                return x.OperadorNombre == nombre;
            });
            if(chof != null){
               var placa : any = _.find(placas,(x:any)=>{
                   return x.value == chof.Placa;
               });
               var placaplata : any = _.find(placasplataforma,(x:any)=>{
                return x.value == chof.PlacaPlataforma;
            });   
             }
            this.setState({U_PTractor :placa  ? placa : selectdefault,U_PPlataforma : placaplata ? placaplata : selectdefault, U_ID :""});
          }}
        />
        <AgregarPlacasTractor
          show={this.state.showModalPlacasTractor}
          handleClose={() => {
            this.setState({ showModalPlacasTractor: false });
          }}
          handleOk={( placa : any) => {
                     var existe = _.find(placas,(x : any)=>{
                         return x.value == placa;
                     });
                     if(existe){
                     alert('Ya existe un registro con el numero de placa');
                        return false;
                     }
                     var plc = {value : placa , label : placa};
                     placas.push(plc);
                     this.setState({U_PTractor :plc , showModalPlacasTractor : false});
          }}
        />
        <AgregarPlacasPlataforma
          show={this.state.showModalPlacasPlataforma}
          handleClose={() => {
            this.setState({ showModalPlacasPlataforma: false });
          }}
          handleOk={( placa : any) => {
                     var existe = _.find(placasplataforma,(x : any)=>{
                         return x.value == placa;
                     });
                     if(existe){
                     alert('Ya existe un registro con el numero de placa');
                        return false;
                     }
                     var plc = {value : placa , label : placa};
                     placasplataforma.push(plc);
                     this.setState({U_PPlataforma :plc , showModalPlacasPlataforma : false});
          }}
        />
        </div>
    }
}

export default connect(
    (state: ApplicationState) => state.RegistroEntradaCamionesStore, RegistroEntradaCamionesStore.actionCreators
)(RegistroEntradaCamiones as any);