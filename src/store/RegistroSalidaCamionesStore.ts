import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as url from '../api';
import * as toastr from '../common/Toast';
export interface RegistroSalidaCamionesStoreState {
    isloadingSalida: boolean,
    registros: any,
    transportistas : any,
    choferes : any,
    foliosPendientes : any,
    folio : any
}

export interface RequestInformacionInicial {
    type: 'REQUEST_INFORMACION_INICIAL_SALIDA'
}

export interface ReceiveInformacionInicial {
    type: 'RECEIVE_INFORMACION_INICIAL_SALIDA',
    registros: any
}
export interface RequestGuardarSalida {
    type: 'REQUEST_GUARDAR_SALIDA'
}

export interface ReceiveIGuardarSalida {
    type: 'RECEIVE_GUARDAR_SALIDA',
    folio: any
}
export interface ReceiveTransportistas {
    type: 'RECEIVE_TRANSPORTISTAS',
    registros: any
}
export interface ReceiveLeerCamiones {
    type: 'RECEIVE_LEER_CAMIONES',
    registros: any
}
export interface requestAddTransportista {
    type: 'ADD_TRANSPORTISTA_S',
    registros: any
}

export interface ReceiveFoliosPendientes{
    type: 'FOLIOS_PENDIENTES',
    registros: any
}
export interface ResetFolio {
    type: 'RESET_FOLIO'
}

type KnownAction = RequestInformacionInicial | ReceiveInformacionInicial
 |RequestGuardarSalida|ReceiveIGuardarSalida|ReceiveTransportistas
 |ReceiveLeerCamiones|requestAddTransportista|ReceiveFoliosPendientes|ResetFolio;

export const actionCreators = {
    requestInformacionInicial: (cliente: string, vendedor: string): AppThunkAction<KnownAction> => (dispatch, getState) => {

    },
    resetFolio: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'RESET_FOLIO'});
    },
    requestInformacionTransportistas: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var transportistas : any= [];
        axios.post(url.LeerTransportistas, { }, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                console.log(data);
                if (data.Type == "success") {
                    if (data.OData.length > 0){
                        transportistas.push({value: '0', label: 'Seleccionar'});
                    }
                   data.OData.map((x : any)=>{
                     transportistas.push({value : x.CardCode, label : x.CardCode + " - " + x.CardName});
                   });
                }
            
                dispatch({ type: 'RECEIVE_TRANSPORTISTAS',registros : transportistas });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_TRANSPORTISTAS', registros: transportistas });
                console.log(error);
            })
    },
    requestFiltrarChofer: (chofer : any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var transportistas : any= [];
        console.log(chofer);
        axios.get(url.LeerCamiones + "/" + chofer,  {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {

                dispatch({ type: 'RECEIVE_LEER_CAMIONES',registros : data.Type == "success" ? data.OData ? data.OData: [] : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_LEER_CAMIONES', registros: [] });
                console.log(error);
            })
    },
    requestGuardarSalida: ( data: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'REQUEST_GUARDAR_SALIDA' });
        axios.post(url.GuardarSalida, data, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                console.log(data);
                if (data.Number >=  0) {
                    dispatch({ type: 'RECEIVE_GUARDAR_SALIDA',folio : data.Number });
                   alert(data.Message);
                }else{
                    dispatch({ type: 'RECEIVE_GUARDAR_SALIDA',folio :"" });
                    alert("Ocurrio un error durante el guardado de los datos."+data.Message);
                }
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_GUARDAR_SALIDA',folio :"" });
                console.log(error);
            })
        
    },
    requestAddTransportista: (transportista : any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var  transportistas : any[] = getState().RegistroSalidaCamionesStore.transportistas;
        transportistas.push(transportista);
        dispatch({ type: 'ADD_TRANSPORTISTA_S', registros: transportistas });
    },
    requestFoliosPendientes: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        axios.post(url.ConsultaFolios, { }, {
           
        })
            .then(response => response.data)
            .then(data => {         
                dispatch({ type: 'FOLIOS_PENDIENTES',registros : data.OData ? data.OData : [] });
            })
            .catch(error => {
                dispatch({ type: 'FOLIOS_PENDIENTES',registros : [] });
                console.log(error);
            })
    },
};

const unloadedState: RegistroSalidaCamionesStoreState = {
    isloadingSalida: false,
    registros: [],
    transportistas :  [],
    choferes : [],
    foliosPendientes :[],
    folio :""
};

export const reducer: Reducer<RegistroSalidaCamionesStoreState> = (state: RegistroSalidaCamionesStoreState | undefined, incomingAction: Action): RegistroSalidaCamionesStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'RESET_FOLIO':
            return {
                ...state,
                folio : ""
            };
        case 'REQUEST_INFORMACION_INICIAL_SALIDA':
            return {
                ...state,
                isloadingSalida: true,
                registros: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_SALIDA':
            return {
                ...state,
                isloadingSalida: false,
                registros: action.registros
            };
            case 'REQUEST_GUARDAR_SALIDA':
            return {
                ...state,
                isloadingSalida: true,
                registros: []
            };
        case 'RECEIVE_GUARDAR_SALIDA':
            return {
                ...state,
                isloadingSalida: false,
                folio: action.folio
            };
            case 'RECEIVE_TRANSPORTISTAS':
                return {
                    ...state,
                    transportistas: action.registros,
                    folio : ""
                };
                case 'RECEIVE_LEER_CAMIONES':
                    return {
                        ...state,
                        choferes : action.registros
                    };
                    case 'ADD_TRANSPORTISTA_S':
                        return {
                            ...state,
                            transportistas : action.registros
                        };
                        case 'FOLIOS_PENDIENTES':
                            return {
                                ...state,
                                foliosPendientes: action.registros
                            };                
        default:
            return state;
    }
};

