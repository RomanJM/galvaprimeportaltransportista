import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';

export interface DescargaCamionesStoreState {
    isloading: boolean,
    registros: any
}

export interface RequestInformacionInicial {
    type: 'REQUEST_INFORMACION_INICIAL_DESCARGA'
}

export interface ReceiveInformacionInicial {
    type: 'RECEIVE_INFORMACION_INICIAL_DESCARGA',
    registros: any
}

type KnownAction = RequestInformacionInicial | ReceiveInformacionInicial;

export const actionCreators = {
    requestInformacionInicial: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
          
    }
    
};

const unloadedState: DescargaCamionesStoreState = {
    isloading: false,
    registros: []
};

export const reducer: Reducer<DescargaCamionesStoreState> = (state: DescargaCamionesStoreState | undefined, incomingAction: Action): DescargaCamionesStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_DESCARGA':
            return {
                ...state,
                isloading: true,
                registros: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_DESCARGA':
            return {
                ...state,
                isloading: false,
                registros: action.registros
            };

        default:
            return state;
    }
};

