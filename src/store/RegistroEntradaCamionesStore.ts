import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as url from '../api';
import * as toastr from '../common/Toast';
import RegistroEntradaCamiones from '../pages/RegistroEntradaCamiones';
export interface RegistroEntradaCamionesStoreState {
    isloadingEntrada: boolean,
    registros: any,
    transportistas : any,
    choferes : any,
    folio :  any
}


export interface RequestGuardarEntrada {
    type: 'REQUEST_GUARDAR_ENTRADA'
}

export interface ReceiveIGuardarEntrada {
    type: 'RECEIVE_GUARDAR_ENTRADA',
    folio: any
}
export interface ReceiveTransportistas {
    type: 'RECEIVE_TRANSPORTISTAS',
    registros: any
}
export interface ReceiveLeerCamiones {
    type: 'RECEIVE_LEER_CAMIONES',
    registros: any
}
export interface AddTransportista {
    type: 'ADD_TRANSPORTISTA',
    registro: any
}
export interface AddChofer {
    type: 'ADD_CHOFER',
    registro: any
}
export interface ResetFolio {
    type: 'RESET_FOLIO'
}

type KnownAction = 
 |RequestGuardarEntrada|ReceiveIGuardarEntrada|ReceiveTransportistas
 |ReceiveLeerCamiones |AddTransportista |AddChofer|ResetFolio;

export const actionCreators = {
    requestInformacionInicial: (cliente: string, vendedor: string): AppThunkAction<KnownAction> => (dispatch, getState) => {

    },
    resetFolio: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'RESET_FOLIO'});
    },
    requestInformacionTransportistas: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var transportistas : any= [];
        axios.post(url.LeerTransportistas, { }, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    if (data.OData.length > 0){
                        transportistas.push({value: '0', label: 'Seleccionar'});
                    }
                   data.OData.map((x : any)=>{
                     transportistas.push({value : x.CardCode, label : x.CardCode + " - " + x.CardName});
                   });
                }
            
                dispatch({ type: 'RECEIVE_TRANSPORTISTAS',registros : transportistas });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_TRANSPORTISTAS', registros: transportistas });
                console.log(error);
            })
    },
    requestFiltrarChofer: (chofer : any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var transportistas : any= [];
        axios.get(url.LeerCamiones + "/" + chofer,  {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {

                dispatch({ type: 'RECEIVE_LEER_CAMIONES',registros : data.Type == "success" ? data.OData ? data.OData: [] : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_LEER_CAMIONES', registros: [] });
                console.log(error);
            })
    },
    requestGuardarEntrada: ( data: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'REQUEST_GUARDAR_ENTRADA' });
        axios.post(url.GuardarEntrada, data, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                console.log(data);
                if (data.Number >=  0) {
                    dispatch({ type: 'RECEIVE_GUARDAR_ENTRADA',folio : data.Number });
                   alert(data.Message);
                }else{
                    dispatch({ type: 'RECEIVE_GUARDAR_ENTRADA',folio : "" });
                    alert("Ocurrio un error durante el guardado de los datos");
                }
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_GUARDAR_ENTRADA',folio : "" });
                console.log(error);
            })
        
    },
    requestAddTransportista: (transportista : any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var  transportistas : any[] = getState().RegistroEntradaCamionesStore.transportistas;
        transportistas.push(transportista);
        dispatch({ type: 'ADD_TRANSPORTISTA', registro: transportistas });
    },
    requestAddChofer: (chofer : any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var  choferes : any[] = getState().RegistroEntradaCamionesStore.choferes;
        choferes.push(chofer);
        dispatch({ type: 'ADD_CHOFER', registro: choferes });
    }
    
};

const unloadedState: RegistroEntradaCamionesStoreState = {
    isloadingEntrada: false,
    registros: [],
    transportistas :  [],
    choferes : [],
    folio : ""
};

export const reducer: Reducer<RegistroEntradaCamionesStoreState> = (state: RegistroEntradaCamionesStoreState | undefined, incomingAction: Action): RegistroEntradaCamionesStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'RESET_FOLIO':
            return {
                ...state,
                folio : ""
            };
            case 'REQUEST_GUARDAR_ENTRADA':
            return {
                ...state,
                isloadingEntrada: true,
                registros: []
            };
        case 'RECEIVE_GUARDAR_ENTRADA':
            return {
                ...state,
                isloadingEntrada: false,
                folio: action.folio
            };
            case 'RECEIVE_TRANSPORTISTAS':
                return {
                    ...state,
                    transportistas: action.registros,
                    folio : ""
                };
                case 'RECEIVE_LEER_CAMIONES':
                    return {
                        ...state,
                        choferes : action.registros
                    };
                    case 'ADD_TRANSPORTISTA':
                        return {
                            ...state,
                            transportistas :  action.registro
   
                  };
                  case 'ADD_CHOFER':
                    return {
                        ...state,
                        choferes :  action.registro

              };
        default:
            return state;
    }
};

