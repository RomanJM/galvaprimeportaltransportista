
import * as RegistroEntradaCamionesStore from './RegistroEntradaCamionesStore';
import * as DescargaCamionesStore from './DescargaCamionesStore';
import * as RegistroSalidaCamionesStore from './RegistroSalidaCamionesStore';

// The top-level state object
export interface ApplicationState {
    RegistroEntradaCamionesStore: RegistroEntradaCamionesStore.RegistroEntradaCamionesStoreState,
    RegistroSalidaCamionesStore: RegistroSalidaCamionesStore.RegistroSalidaCamionesStoreState,
    DescargaCamionesStore : DescargaCamionesStore.DescargaCamionesStoreState
}

// Whenever an action is dispatched, Redux will update each top-level application state property using
// the reducer with the matching name. It's important that the names match exactly, and that the reducer
// acts on the corresponding ApplicationState property type.
export const reducers = {
    RegistroEntradaCamionesStore: RegistroEntradaCamionesStore.reducer,
    DescargaCamionesStore : DescargaCamionesStore.reducer,
    RegistroSalidaCamionesStore : RegistroSalidaCamionesStore.reducer
};

// This type can be used as a hint on action creators so that its 'dispatch' and 'getState' params are
// correctly typed to match your store.
export interface AppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}
